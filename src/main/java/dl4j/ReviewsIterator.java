package dl4j;

import org.apache.commons.io.FileUtils;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Asus on 6/1/2017.
 */
public class ReviewsIterator implements DataSetIterator{

    private final WordVectors wordVectors;
    private final int batchSize;
    private final int vectorSize;
    private final int truncateLength;

    private int cursor = 0;
    private final File[] negativeReviews;
    private final File[] positiveReviews;
    private final File[] neutralReviews;
    private final TokenizerFactory tokenizerFactory;

    public ReviewsIterator(WordVectors wordVectors, int batchSize, int truncateLength) {
        this.wordVectors = wordVectors;
        this.vectorSize = wordVectors.getWordVector(wordVectors.vocab().wordAtIndex(0)).length;
        this.batchSize = batchSize;
        this.truncateLength = truncateLength;
        File positives = new File("C:\\Folder\\Lic\\Data\\positives\\");
        File negatives = new File("C:\\Folder\\Lic\\Data\\negatives\\");
        File neutrals = new File("C:\\Folder\\Lic\\Data\\neutrals\\");

        negativeReviews = negatives.listFiles();
        positiveReviews = positives.listFiles();
        neutralReviews = neutrals.listFiles();

        tokenizerFactory = new DefaultTokenizerFactory();
        tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());
    }

    @Override
    public DataSet next(int i) {
        if (cursor >= positiveReviews.length + negativeReviews.length + neutralReviews.length)
            throw new NoSuchElementException();
        try {
            return nextDataSet(i);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private DataSet nextDataSet(int n) throws IOException {
        //Load reviews to String.
        //Alteranate positive, negative and neutrals
        List<String> reviews = new ArrayList<>();
        int[] polarity = new int[n];
        int polarityIndex = 0;
        int variable = 0;
        int reviewsActualSize = 0;
        int ex = 0;
       // int addedReviewAtThisIteration = 0;
        int positives = 0;
        int negatives = 0;
        int neutrals = 0;
        for (int i = 0; i < n && cursor < totalExamples(); i++) {
            int addedReviewAtThisIteration = 0;
            ex = cursor / 3;
            if (positives < positiveReviews.length) {
                String positiveReview = FileUtils.readFileToString(positiveReviews[positives]);
                polarity[reviews.size()] = 2;
                reviews.add(positiveReview);

                addedReviewAtThisIteration ++;
                positives ++;
            }
            if (negatives < negativeReviews.length) {
                String negativeReview = FileUtils.readFileToString(negativeReviews[negatives]);
                polarity[reviews.size()] = 0;
                reviews.add(negativeReview);

                addedReviewAtThisIteration ++;
                negatives ++;
            }
                if (neutrals < neutralReviews.length) {
                    String neutralReview = FileUtils.readFileToString(neutralReviews[neutrals]);
                    polarity[reviews.size()] = 1;
                    reviews.add(neutralReview);

                    addedReviewAtThisIteration ++;
                    neutrals ++;
                }

                i += addedReviewAtThisIteration - 1 ;


        }

        //tokenize reviews and filter out unknown words, or intiialize them randomly
        List<List<String>> allTokens = new ArrayList<>(reviews.size());
        int maxLength = 0;
        for (String s : reviews) {
            List<String> tokens = tokenizerFactory.create(s).getTokens();//check what this means
            List<String> tokensFiltered = new ArrayList<>();
            for (String t : tokens) {
                if (wordVectors.hasWord(t)) tokensFiltered.add(t);
            }
            allTokens.add(tokensFiltered);
            maxLength = Math.max(maxLength, tokensFiltered.size());
        }

            //not sure if this is needed
            if (maxLength > truncateLength)
            maxLength = truncateLength;

            //Create data for training
            INDArray features = Nd4j.create(reviews.size(), vectorSize, maxLength);
            INDArray labels = Nd4j.create(reviews.size(), 3, maxLength);

            //here we add the padding tokens: 0 padding
            INDArray featureMask = Nd4j.zeros(reviews.size(), maxLength);
            INDArray labelsMask = Nd4j.zeros(reviews.size(), maxLength);

            int[] temp = new int[2];
            for (int i = 0; i < reviews.size(); i ++) {
                //for cycle to loop through each review i
                List<String> tokens = allTokens.get(i);
                temp[0] = i;// temp holds the review i and the token j
                for (int j = 0; j < tokens.size() && j < maxLength; j ++) {
                    // loop through each token of the review j
                    String token = tokens.get(j);
                    INDArray vector = wordVectors.getWordVectorMatrix(token); //get the word vector of the token
                    features.put(new INDArrayIndex[] {NDArrayIndex.point(i), NDArrayIndex.all(), NDArrayIndex.point(j)}, vector);

                    temp[1] = j;
                    featureMask.putScalar(temp, 1.0); //not padding for this example + time stp
            }
            int polarityValue = polarity[i];
                int lastIdx = Math.min(tokens.size(), maxLength);
                labels.putScalar(new int[]{i, polarityValue, lastIdx-1}, 1.0); //there will be a 1 for the correpsonding position of the polarity
                //positive: [0 0 1]
                //neutral: [0 1 0]
                //negative: [1 0 0]
                labelsMask.putScalar(new int[]{i, lastIdx-1}, 1.0);
        }
        return new DataSet(features, labels, featureMask, labelsMask);

    }

    @Override
    public int totalExamples() {
        return positiveReviews.length + neutralReviews.length + negativeReviews.length;
    }

    @Override
    public int inputColumns() {
        return vectorSize;
    }

    @Override
    public int totalOutcomes() {
        return 3;
    }

    @Override
    public boolean resetSupported() {
        return true;
    }

    @Override
    public boolean asyncSupported() {
        return true;
    }

    @Override
    public void reset() {
        cursor = 0;
    }

    @Override
    public int batch() {
        return batchSize;
    }

    @Override
    public int cursor() {
        return cursor;
    }

    @Override
    public int numExamples() {
        return totalExamples();
    }

    @Override
    public void setPreProcessor(DataSetPreProcessor dataSetPreProcessor) {
        throw new UnsupportedOperationException();
    }

    @Override
    public DataSetPreProcessor getPreProcessor() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> getLabels() {
        return Arrays.asList("positive", "negative", "neutral");
    }

    @Override
    public boolean hasNext() {
        return cursor < numExamples();
    }

    @Override
    public DataSet next() {
        return next(batchSize);
    }

    @Override
    public void remove(){

    }
}
