package dl4j;

import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.GravesBidirectionalLSTM;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.text.sentenceiterator.LineSentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

/**
 * Created by Asus on 5/9/2017.
 */
public class BiLSTMModel {

    public static final String WORD_VECTORS_PATH = "C:\\Folder\\Lic\\GoogleNews-vectors-negative300.bin.gz";

    public static void main(String[] args) {
        int wordVectorSize = 300; //this is the size of the word vectors
        WordVectors wordVectors = WordVectorSerializer.loadStaticModel(new File(WORD_VECTORS_PATH));

        int lstmLayerSize = 300; //this is the layer size of the LSTM
        int nOut = 3; //output is of size three for positive, negative, neutral

        MultiLayerConfiguration conf = createMultilayerNetworkConfiguration(wordVectorSize, lstmLayerSize, nOut);
        MultiLayerNetwork multiLayerNetwork = new MultiLayerNetwork(conf);
        multiLayerNetwork.init();
        multiLayerNetwork.setListeners(new ScoreIterationListener((1))); //prints out the score at every iteration

        int batchSize = 50;//359;//smaller mini-batches would be better but for now use this
        Random rng = new Random(12345);
        DataSetIterator trainingSet = new ReviewsIterator(wordVectors, batchSize, 55);
        DataSetIterator testingSet = new ReviewsIterator(wordVectors, batchSize, 55);
        System.out.println("Start training");
        int epochs = 1; //will increase this value later
        for (int i = 0; i < epochs; i++) {
            trainModel(multiLayerNetwork, trainingSet);
            System.out.println("Epoch " + i + " complete. Starting evaluation: ");
            testModel(multiLayerNetwork, testingSet);
        }
    }

    private static MultiLayerConfiguration createMultilayerNetworkConfiguration(int wordVectorSize, int lstmLayerSize, int nOut) {
        return new NeuralNetConfiguration.Builder()
                    .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                    .learningRate(0.1)
                    .rmsDecay(0.95)
                    .seed(12345)
                    .regularization(false)
                    .weightInit(WeightInit.XAVIER)
                    .updater(Updater.ADAM)
                    .list()
                    .layer(0, new GravesLSTM.Builder().nIn(wordVectorSize).nOut(55).activation(Activation.TANH).build())
                    //.layer(1, new GravesBidirectionalLSTM.Builder().nIn(lstmLayerSize).nOut(lstmLayerSize).activation(Activation.TANH).build())
                    .layer(1, new RnnOutputLayer.Builder(LossFunctions.LossFunction.MCXENT).activation(Activation.SOFTMAX).nIn(55).nOut(nOut).build())
                    .pretrain(false)
                    .backprop(true)
                    .build();
    }

    private static void trainModel(MultiLayerNetwork multiLayerNetwork, DataSetIterator trainingSet) {
        multiLayerNetwork.fit(trainingSet);
    }

    private static void testModel(MultiLayerNetwork multiLayerNetwork, DataSetIterator testingSet) {
        Evaluation evaluation = multiLayerNetwork.evaluate(testingSet);
        System.out.println(evaluation.accuracy());
        System.out.println(evaluation.precision());
        System.out.println(evaluation.recall());
        System.out.println(evaluation.f1());
        System.out.println(evaluation.stats());
    }
}

