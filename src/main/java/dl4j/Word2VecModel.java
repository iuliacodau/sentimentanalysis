package dl4j;

import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.sentenceiterator.LineSentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Collection;

/**
 * Created by Asus on 6/1/2017.
 */
public class Word2VecModel {

    private static Logger log = LoggerFactory.getLogger(BiLSTMModel.class);
    private static File textDataset = new File("D:\\Licenta\\Implementare\\Data\\review_texts.txt");
    private static File wordVectorsFile = new File("D:\\Licenta\\Implementare\\Data\\word_vectors.txt");
    private final static String vectorsFilePath = "D:\\Licenta\\Implementare\\Data\\word_vectors.txt";
    private final static String vectorsFilePath2 = "D:\\Licenta\\Implementare\\Data\\word_vectors2.txt";
    public static final String WORD_VECTORS_PATH = "D:\\Licenta\\Implementare\\GoogleNews-vectors-negative300.bin.gz";

    public static void main(String[] args){
        SentenceIterator iterator = new LineSentenceIterator(textDataset);
        //this assumes that each line in the file is a sentence
/*        iterator.setPreProcessor(new SentencePreProcessor() {
            @Override
            public String preProcess(String s) {
                return s.toLowerCase();
            }
        });*/
        log.info("Tokenizing dataset...");
        TokenizerFactory tokenizerFactory = new DefaultTokenizerFactory();
        //DefaultTokenizerFactory or NGramTokenizerFactory
        tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());
        log.info("Building the model...");
        Word2Vec w2vNet = new Word2Vec.Builder()
                .minWordFrequency(1)
                //if a word appears less than 3 times, it is not learned
                .iterations(3)
                //number of times you allow the net to update its coefficients for one batch of the data
                .layerSize(300)
                //number of dimensions in the featurespace
                .seed(42)
                .windowSize(5)
                .epochs(5)
                .learningRate(0.005)
                .iterate(iterator)
                .tokenizerFactory(tokenizerFactory)
                .build();
        log.info("Fitting the model...");
        w2vNet.fit();
        //tells the configured net to begin training
        log.info("Writing word vectors...");
        /*try {*/
        //WordVectorSerializer.writeWordVectors(w2vNet, wordVectorsFile);
        //check what is not deprecated
        //update POM to newest version
        // WordVectorSerializer.writeWordVectors(w2vNet, vectorsFilePath);
        WordVectorSerializer.writeWord2VecModel(w2vNet, vectorsFilePath2);
        log.info("Closest words:");
        Collection<String> listForGood = w2vNet.wordsNearest("judging", 10);
        System.out.println(listForGood);
        log.info("Cosine similarity:");
        double cosineSimilarity = w2vNet.similarity("taste", "bad");
        System.out.println(cosineSimilarity);
        /*} catch (IOException e) {
            e.printStackTrace();
        }*/
    }
}
